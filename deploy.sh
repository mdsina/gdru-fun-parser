#!/usr/bin/env bash

echo "Deploying GD.RU fun parser"

sshpass -p "$DEPLOY_PASS" scp -P 2222 target/fun-parser-1.0-SNAPSHOT.jar $DEPLOY_USER@$DEPLOY_HOST:$DEPLOY_DIR
sshpass -p "$DEPLOY_PASS" ssh -p 2222 $DEPLOY_USER@$DEPLOY_HOST service fun-parser restart