CREATE TABLE threads
(
  id SERIAL PRIMARY KEY NOT NULL,
  url VARCHAR(2000) NOT NULL
);
CREATE UNIQUE INDEX threads_id_uindex ON threads (id);
CREATE UNIQUE INDEX threads_url_uindex ON threads (url);
COMMENT ON TABLE threads IS 'Threads to parse';

CREATE TABLE last_parsed
(
  thread_id INT NOT NULL,
  page INT NOT NULL,
  message INT NOT NULL,
  CONSTRAINT last_parsed_THREADS_ID_fk FOREIGN KEY (thread_id) REFERENCES threads (ID)
);
CREATE UNIQUE INDEX last_parsed_thread_id_uindex ON last_parsed (thread_id);