package com.mds.gdru.resources;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexControllerResource {

    @GetMapping("/")
    public String hello() {
        return "Hello World!";
    }
}
