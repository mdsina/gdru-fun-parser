package com.mds.gdru.components;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Profile("heroku")
public class HerokuPing {

    private static final Logger logger = LoggerFactory.getLogger(HerokuPing.class);

    @Value("${heroku.url}")
    private String herokuUrl;

    @Scheduled(fixedRate = 1200000L)
    public void ping() throws IOException {
        logger.info("Ping {}..", herokuUrl);
        Jsoup.connect(herokuUrl).get();
        logger.info("Ping {} completed.", herokuUrl);
    }
}
