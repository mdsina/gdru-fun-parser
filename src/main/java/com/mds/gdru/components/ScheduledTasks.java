package com.mds.gdru.components;


import com.mds.gdru.jpa.model.Thread;
import com.mds.gdru.jpa.repository.ParsedThreadRepository;
import com.mds.gdru.jpa.repository.ThreadRepository;
import com.mds.gdru.tasks.PageParseTask;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.gpedro.integrations.slack.SlackApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class ScheduledTasks
{
    private final @NonNull ThreadRepository threadRepository;
    private final @NonNull ParsedThreadRepository parsedThreadRepository;
    private final @NonNull SlackApi slackApi;

    @Value("${gdru.domain}")
    private String gdruBaseUrl;

    @Scheduled(fixedRate = 300000L)
    public void parseThreads() {
        List<Thread> threads = threadRepository.getCachedThreads();
        threads.stream()
            .map(thread -> new PageParseTask(slackApi, parsedThreadRepository, gdruBaseUrl, thread))
            .forEach(PageParseTask::run);
    }
}
