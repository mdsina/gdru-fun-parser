package com.mds.gdru.components;

import net.gpedro.integrations.slack.SlackApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class SlackClientProvider
{
    @Value("${slack.webhook.url}")
    private String webhookUrl;

    @Bean
    public SlackApi slackApi() {
        return new SlackApi(webhookUrl);
    }
}
