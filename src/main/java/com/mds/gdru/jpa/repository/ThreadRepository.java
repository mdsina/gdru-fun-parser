package com.mds.gdru.jpa.repository;


import com.mds.gdru.jpa.model.Thread;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ThreadRepository extends JpaRepository<Thread, Integer>
{
    @Cacheable("threads")
    default List<Thread> getCachedThreads() {
        return findAll();
    }
}
