package com.mds.gdru.jpa.repository;

import com.mds.gdru.jpa.model.Thread;
import com.mds.gdru.jpa.model.ParsedThread;
import com.mds.gdru.jpa.model.ParsedThreadPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParsedThreadRepository extends JpaRepository<ParsedThread, ParsedThreadPK>
{
    ParsedThread getByParsedThreadPKThread(Thread thread);
}
