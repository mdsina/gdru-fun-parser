package com.mds.gdru.jpa.model;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "last_parsed")
public class ParsedThread implements Serializable
{
    @EmbeddedId
    private ParsedThreadPK parsedThreadPK;

    @Column(name = "page")
    private Integer page;

    @Column(name = "message")
    private Integer message;

    public ParsedThread() {
    }

    public ParsedThread(Thread thread) {
        this.parsedThreadPK = new ParsedThreadPK(thread);
    }

    public ParsedThread(Thread thread, Integer page, Integer message) {
        this.parsedThreadPK = new ParsedThreadPK(thread);
        this.page = page;
        this.message = message;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getMessage() {
        return message;
    }

    public void setMessage(Integer message) {
        this.message = message;
    }

    public Thread getThread() {
        return parsedThreadPK.getThread();
    }
}
