package com.mds.gdru.jpa.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "threads")
public class Thread implements Serializable
{
    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "url")
    private String url;

    public Thread() {
    }

    public Thread(Integer id, String url) {
        this.id = id;
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
