package com.mds.gdru.jpa.model;


import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

@Embeddable
public class ParsedThreadPK implements Serializable
{
    @OneToOne
    @JoinColumn(name = "thread_id")
    private Thread thread;

    public ParsedThreadPK() {
    }

    public ParsedThreadPK(Thread thread) {
        this.thread = thread;
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public Thread getThread() {
        return thread;
    }
}
