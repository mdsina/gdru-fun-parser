package com.mds.gdru.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class AppConfig
{
    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setCorePoolSize(1);
        pool.setMaxPoolSize(3);
        pool.setThreadNamePrefix("gdru-tpool-");
        pool.setQueueCapacity(5);
        pool.setWaitForTasksToCompleteOnShutdown(true);
        return pool;
    }
}
