package com.mds.gdru.config;

import com.mds.gdru.jpa.model.Thread;
import com.mds.gdru.jpa.repository.ParsedThreadRepository;
import com.mds.gdru.tasks.PageParseTask;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.gpedro.integrations.slack.SlackApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class TasksConfig
{
    private final @NonNull ParsedThreadRepository parsedThreadRepository;
    private final @NonNull SlackApi slackApi;

    @Value("${gdru.domain}")
    private String gdruBaseUrl;

    @Bean
    @Scope("prototype")
    public PageParseTask pageParseTask(Thread thread) {
        return new PageParseTask(slackApi, parsedThreadRepository, gdruBaseUrl, thread);
    }
}
