package com.mds.gdru.tasks.slack;

import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class VideosAccumulator extends BaseAccumulator
{
    private static final Logger logger = LoggerFactory.getLogger(VideosAccumulator.class);

    private List<String> videos = new ArrayList<>();

    public VideosAccumulator(SlackApi slackApi) {
        super(slackApi, new SlackMessage());
        slackMessage.setUnfurlLinks(true);
        slackMessage.setUnfurlMedia(true);
    }

    public void add(String videoUrl, String ignored) {
        String url = videoUrl;
        if (url.startsWith("//")) {
            url = "https:" + url; // ¯\_(ツ)_/¯
        }
        url = url.replace("embed/", "watch?v=");
        videos.add("<" + url + ">");
    }

    public void add(String imageUrl) {
        add(imageUrl, "");
    }

    public void flush() {
        if (!videos.isEmpty()) {
            slackMessage.setText(StringUtils.join(videos, '\n'));
            try {
                slackApi.call(slackMessage);
            } catch (Exception e) {
                logger.error("Cannot sent request to slack: {}", e.getMessage(), e);
            }
        }
        slackMessage = new SlackMessage("");
        slackMessage.setUnfurlLinks(true);
        slackMessage.setUnfurlMedia(true);
        videos.clear();
    }
}
