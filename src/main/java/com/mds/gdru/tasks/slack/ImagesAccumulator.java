package com.mds.gdru.tasks.slack;

import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackAttachment;
import net.gpedro.integrations.slack.SlackMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ImagesAccumulator extends BaseAccumulator
{
    private static final Logger logger = LoggerFactory.getLogger(ImagesAccumulator.class);

    private List<SlackAttachment> attachments = new ArrayList<>();

    public ImagesAccumulator(SlackApi slackApi) {
        super(slackApi, new SlackMessage());
        slackMessage.setText("");
    }

    public ImagesAccumulator(SlackApi slackApi, SlackMessage slackMessage) {
        super(slackApi, slackMessage);
        slackMessage.setText("");
    }

    public void add(String imageUrl, String text) {
        SlackAttachment attachment = new SlackAttachment();
        attachment.setImageUrl(imageUrl);
        attachment.setFallback(imageUrl);
        attachment.setText(text);
        attachments.add(attachment);
    }

    public void add(String imageUrl) {
        add(imageUrl, "");
    }

    public void flush() {
        if (!attachments.isEmpty()) {
            slackMessage.setAttachments(attachments);
            try {
                slackApi.call(slackMessage);
            } catch (Exception e) {
                logger.error("Cannot sent request to slack: {}", e.getMessage(), e);
            }
        }
        slackMessage = new SlackMessage("");
        attachments.clear();
    }
}
