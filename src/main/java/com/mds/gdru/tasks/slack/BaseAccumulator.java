package com.mds.gdru.tasks.slack;

import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;

abstract class BaseAccumulator
{
    protected final SlackApi slackApi;
    protected SlackMessage slackMessage;

    public BaseAccumulator(SlackApi slackApi) {
        this(slackApi, new SlackMessage());
        slackMessage.setText("");
    }

    public BaseAccumulator(SlackApi slackApi, SlackMessage slackMessage) {
        this.slackMessage = slackMessage;
        this.slackApi = slackApi;
    }

    abstract public void add(String content);
    abstract public void add(String content, String fallback);

    abstract public void flush();
}
