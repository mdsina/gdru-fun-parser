package com.mds.gdru.tasks;

import com.mds.gdru.jpa.model.ParsedThread;
import com.mds.gdru.jpa.model.Thread;
import com.mds.gdru.jpa.repository.ParsedThreadRepository;
import com.mds.gdru.tasks.slack.ImagesAccumulator;
import com.mds.gdru.tasks.slack.VideosAccumulator;
import net.gpedro.integrations.slack.SlackApi;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class PageParseTask implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(PageParseTask.class);

    private static final String MESSAGE_SELECTOR = ">div:has(.mes)";

    private final ParsedThreadRepository parsedThreadRepository;
    private final SlackApi slackApi;

    private final Thread thread;
    private final String baseUrl;

    public PageParseTask(
        SlackApi slackApi,
        ParsedThreadRepository parsedThreadRepository,
        String baseUrl,
        Thread thread
    ) {
        this.slackApi = slackApi;
        this.parsedThreadRepository = parsedThreadRepository;

        this.thread = thread;
        this.baseUrl = baseUrl;
    }

    private void saveParsedThread(ParsedThread parsedThread, int page, int message) {
        parsedThread.setPage(page);
        parsedThread.setMessage(message);

        parsedThreadRepository.saveAndFlush(parsedThread);
    }

    private int parseMessage(Element messageNode) {
        String last = messageNode.select(">table td:last-child a").text();
        return Integer.valueOf(StringUtils.strip(last.trim(), "#"));
    }

    private String buildFetchUrl(String threadBaseUrl, int page) {
        return threadBaseUrl + "&page=" + String.valueOf(page);
    }

    private Optional<Map<String, Object>> parsePage(String url) {
        Map<String, Object> result = new HashMap<>();

        Document doc;

        try {
            logger.debug("Start getting page body...");
            doc = Jsoup.connect(url).get();
            logger.debug("Page body retrieved.");
        } catch (IOException e) {
            logger.warn("Exception occurred. Cannot parse url '{}' : {}", url, e.getMessage());
            return Optional.empty();
        }

        Elements mainBody = doc.select("#main_body");
        Elements pagingElements = mainBody.select("div.pages");

        if (!pagingElements.isEmpty()) {
            Element pagingElement = pagingElements.get(0);
            Element currentPageElem = pagingElement.select("> span").get(0);
            int currentPage = Integer.valueOf(currentPageElem.text().trim());

            Elements nextLinks = pagingElement.select("a:contains(Следующая)");
            if (!nextLinks.isEmpty()) {
                Element element = nextLinks.get(0);
                result.put("next-link", element.attr("href"));
                result.put("last-page", Integer.valueOf(element.previousElementSibling().text()));
            }

            if (currentPageElem.nextElementSibling() != null) {
                result.put("next-page", Integer.valueOf(currentPageElem.nextElementSibling().text()));
            }

            result.put("current-page", currentPage);
        } else {
            result.put("current-page", 1);
        }

        result.put("messages", mainBody.select(MESSAGE_SELECTOR));

        return Optional.of(result);
    }

    private void updateLastParsedMessage(final String threadUrl) {
        Optional<Map<String, Object>> resultHolder = parsePage(threadUrl);
        if (!resultHolder.isPresent()) {
            return;
        }

        Map<String, Object> result = resultHolder.get();

        ParsedThread parsedThread = new ParsedThread(thread);

        Optional<Integer> lastPageHolder = Optional.ofNullable(result.get("next-page")).map(o -> (Integer) o);
        Elements messages = (Elements) result.get("messages");

        if (!lastPageHolder.isPresent()) {
            logger.warn("Cannot retrieve paging element. Looks like there are only one page");

            saveParsedThread(parsedThread, 1, parseMessage(messages.last()));

            return;
        }

        Integer nextPage = lastPageHolder.get();
        Integer currentPage = (Integer) result.get("current-page");
        if (currentPage < nextPage) {
            Optional<Map<String, Object>> lastResultHolder = parsePage(buildFetchUrl(threadUrl, nextPage));
            if (!lastResultHolder.isPresent()) {
                return;
            }
            messages = (Elements) lastResultHolder.get().get("messages");
        }

        saveParsedThread(parsedThread, nextPage, parseMessage(messages.last()));

        logger.info("Initialize for parsing thread '{}': page={}, message={}", threadUrl, nextPage, parsedThread.getMessage());
    }

    @Override
    public void run() {
        final String threadUrl = baseUrl + thread.getUrl();

        logger.info("Start parsing thread {}", threadUrl);

        ParsedThread one = parsedThreadRepository.getByParsedThreadPKThread(thread);
        if (one == null) {
            updateLastParsedMessage(threadUrl);
            return;
        }

        Optional<Map<String, Object>> resultHolder = parsePage(buildFetchUrl(threadUrl, one.getPage()));
        if (!resultHolder.isPresent()) {
            return;
        }
        Map<String, Object> result = resultHolder.get();

        int message = one.getMessage();
        final int currentMessage = message;

        ImagesAccumulator imagesAccumulator = new ImagesAccumulator(slackApi);
        VideosAccumulator videosAccumulator = new VideosAccumulator(slackApi);

        List<Element> messages = ((Elements) result.get("messages"))
            .stream()
            .filter(element -> parseMessage(element) > currentMessage)
            .collect(Collectors.toList());

        for (Element messageElement : messages) {
            final String messageUrl = messageElement.select(">table>tbody>tr>td:last-child a").get(0).attr("href");

            message = parseMessage(messageElement);

            messageElement.select("img").forEach(img -> imagesAccumulator.add(img.attr("src"), messageUrl));
            messageElement.select("iframe").forEach(frame -> videosAccumulator.add(frame.attr("src")));
        }

        imagesAccumulator.flush();
        videosAccumulator.flush();

        int nextPage = (Integer) result.get("current-page");
        if (result.containsKey("next-page")) {
            nextPage = (Integer) result.get("next-page");
            logger.info("Page parsed. Next page: {}", nextPage);
        }

        saveParsedThread(one, nextPage, message);

        logger.info("End parsing thread {}", threadUrl);
    }
}
